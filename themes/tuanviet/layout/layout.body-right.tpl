<!-- BEGIN: main -->
{FILE "header_only.tpl"}
{FILE "header_extended.tpl"}
<style type="text/css">
	body{
		font-size:14px;
	}
	.slider-sidebar{
		margin-bottom: 10px;
	}
	@media (max-width:320px){
	.menu1{
		width: 100%;
	}
	.menu2{
		width: 100%;
	}
	}
	@media(min-width:321px) and (max-width:360px){
		.menu1{
		width: 100%;
	}
	.menu2{
		width: 100%;
	}
	}
	@media (min-width:361px) and (max-width:480px){
			.menu1{
		width: 100%;
	}
	.menu2{
		width: 100%;
	}
	}
</style>
<div class="row">
	<div class="col-md-24">
		[HEADER]
	</div>
</div>
<div class="row">
	<div class="col-sm-24 col-md-17 content_new">
		<div class="breadcrumb">
	<nav class="third-nav">
		<div class="row">
			<div class="bg">
				<div class="clearfix">
					<div class="col-sm-16 col-md-18">
						<!-- BEGIN: breadcrumbs -->
						<div class="breadcrumbs-wrap">
							<div class="display">
								<a class="show-subs-breadcrumbs hidden" href="#" onclick="showSubBreadcrumbs(this, event);"><em class="fa fa-lg fa-angle-right"></em></a>
								<ul class="breadcrumbs list-none">
								</ul>
							</div>
							<ul class="subs-breadcrumbs">
							</ul>
							<ul class="temp-breadcrumbs hidden">
								<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
									<a href="{THEME_SITE_HREF}" itemprop="url" title="{LANG.Home}"><span itemprop="title">{LANG.Home}</span></a>
								</li>
								<!-- BEGIN: loop -->
								<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
									<a href="{BREADCRUMBS.link}" itemprop="url" title="{BREADCRUMBS.title}"><span class="txt" itemprop="title">{BREADCRUMBS.title}</span></a>
								</li><!-- END: loop -->
							</ul>
						</div>
						<!-- END: breadcrumbs -->
					</div>
				</div>
			</div>
		</div>
	</nav>
</div>
		[TOP]
		{MODULE_CONTENT}
		[BOTTOM]
	</div>
	<div class="col-sm-24 col-md-7 menu-device">
		<div class="col-xs-8 col-sm-8 col-md-24 menu1">
			[RIGHT]
		</div>
		<div class="col-xs-8 col-sm-8 col-md-24 menu2">
			[RIGHT1]
		</div>

	</div>
</div>
<div class="row">
		[FOOTER]
</div>
{FILE "footer_extended.tpl"}
{FILE "footer_only.tpl"}
<!-- END: main -->