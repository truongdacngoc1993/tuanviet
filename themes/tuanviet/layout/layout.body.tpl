<!-- BEGIN: main -->
{FILE "header_only.tpl"}
{FILE "header_extended.tpl"}
<div class="row">
	<div class="col-md-24">
		<div class="prize">
			[PRIZE]
		</div>
		<div class="top-hit">
		[TOP]
		</div>
		{MODULE_CONTENT}
		<div class="bottom-content hidden-xs hidden-sm">
			<div class="col-md-13">
				<div class="bottom-left">
				[BOTTOM-LEFT]
				</div>
			</div>
			<div class="col-md-11">
				<div class="bottom-right">
					[BOTTOM-RIGHT]
				</div>
			</div>
		</div>
		<div class="col-md-15 col-sm-15 comment">[COMMENT]</div>
		<div class="col-md-9 col-sm-9 video">[VIDEO_CLIPS]</div>
	</div>
</div>
{FILE "footer_extended.tpl"}
{FILE "footer_only.tpl"}
<!-- END: main -->