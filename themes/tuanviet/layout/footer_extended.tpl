                </div>
            </section>
        </div>
        <footer id="footer">
            <div class="wraper">
                <div class="container">
                    <div class="row">
                    	<div class="partner">
						[PARTNER]
						</div>
                    </div>
                </div>
            </div>
        </footer>
        <nav class="footerNav2">
            <div class="wraper hidden-xs hidden-sm">
					[COMPANY_INFO]
            </div>
            <div class="wraper hidden-md">
            	[COMPANY_BUTTON]
            </div>
        </nav>
        {ADMINTOOLBAR}
    </div>
    <!-- SiteModal Required!!! -->
    <div id="sitemodal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <em class="fa fa-spinner fa-spin">&nbsp;</em>
                </div>
                <button type="button" class="close" data-dismiss="modal"><span class="fa fa-times"></span></button>
            </div>
        </div>
    </div>
	<div class="fix_banner_left">
		[FIX_BANNER_LEFT]
	</div>
	<div class="fix_banner_right">
		[FIX_BANNER_RIGHT]
	</div>