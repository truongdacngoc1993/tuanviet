<!-- BEGIN: main -->
{FILE "header_only.tpl"}
{FILE "header_extended.tpl"}
<style type="text/css">
	body{
		font-size:14px;
	}
	.slider-sidebar li {
    border: 1px solid #ddd;
    box-shadow: 0 0 2px #ddd;
    display: inline;
    float: left;
    height: 115px !important;
    margin-bottom: 5px;
    margin-right: 5px;
    overflow: hidden;
    padding: 5px;
    width: 48%;
}
@media (min-width:981px) and (max-width:1024px) {
	.list-unstyled li{
		float:left;
		width: 47%;
	}
	.slider-sidebar li{
		height: 108px;
	}
	.last-post .entry-title {
    background: #1b93df none repeat scroll 0 0;
    text-align:center;
    min-height: 50px;
    padding: 10px;
    width:100%;
   }
}
</style>
<div class="breadcrumb">
	<nav class="third-nav">
		<div class="row">
			<div class="bg">
				<div class="clearfix">
					<div class="col-xs-24 col-sm-18 col-md-18">
						<!-- BEGIN: breadcrumbs -->
						<div class="breadcrumbs-wrap">
							<div class="display">
								<a class="show-subs-breadcrumbs hidden" href="#" onclick="showSubBreadcrumbs(this, event);"><em class="fa fa-lg fa-angle-right"></em></a>
								<ul class="breadcrumbs list-none">
								</ul>
							</div>
							<ul class="subs-breadcrumbs">
							</ul>
							<ul class="temp-breadcrumbs hidden">
								<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
									<a href="{THEME_SITE_HREF}" itemprop="url" title="{LANG.Home}"><span itemprop="title">{LANG.Home}</span></a>
								</li>
								<!-- BEGIN: loop -->
								<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
									<a href="{BREADCRUMBS.link}" itemprop="url" title="{BREADCRUMBS.title}"><span class="txt" itemprop="title">{BREADCRUMBS.title}</span></a>
								</li><!-- END: loop -->
							</ul>
						</div>
						<!-- END: breadcrumbs -->
					</div>
				</div>
			</div>
		</div>
	</nav>
</div>
<div class="title-br">
	<h3 class="wrapper-title-center">{BREADCRUMBS.title}</h3>
</div>
<div class="row">
	<div class="col-md-24">
		[TOP]
		{MODULE_CONTENT}
		[BOTTOM]
	</div>
</div>
<div class="row">
	[FOOTER]
</div>
{FILE "footer_extended.tpl"}
{FILE "footer_only.tpl"}
<!-- END: main -->