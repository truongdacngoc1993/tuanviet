<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2014 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Sun, 04 May 2014 12:41:32 GMT
 */

if( ! defined( 'NV_MAINFILE' ) ) die( 'Stop!!!' );

if( ! nv_function_exists( 'nv_menu_theme_content_left' ) )
{
	function nv_menu_theme_content_left_config( $module, $data_block, $lang_block )
	{
		$html = '<tr>';
		$html .= '	<td>' . $lang_block['title1'] . '</td>';
		$html .= '	<td><input type="text" name="config_title1" class="form-control" value="' . $data_block['title1'] . '"/></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['description1'] . '</td>';
		$html .= '	<td><input type="text" name="config_description1" class="form-control" value="' . $data_block['description1'] . '"/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['description2'] . '</td>';
		$html .= '	<td><input type="text" name="config_description2" class="form-control" value="' . $data_block['description2'] . '"/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['description3'] . '</td>';
		$html .= '	<td><input type="text" name="config_description3" class="form-control" value="' . $data_block['description3'] . '"/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['description4'] . '</td>';
		$html .= '	<td><input type="text" name="config_description4" class="form-control" value="' . $data_block['description4'] . '"/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['description5'] . '</td>';
		$html .= '	<td><input type="text" name="config_description5" class="form-control" value="' . $data_block['description5'] . '"/></td>';
		$html .= '</tr>';

		return $html;
	}

	function nv_menu_theme_content_left_submit( $module, $lang_block )
	{
		global $nv_Request;
		$return = array();
		$return['error'] = array();

		$return['config']['title1'] = $nv_Request->get_title( 'config_title1', 'post' );

		$return['config']['description1'] = $nv_Request->get_title( 'config_description1', 'post' );


		$return['config']['description2'] = $nv_Request->get_title( 'config_description2', 'post' );


		$return['config']['description3'] = $nv_Request->get_title( 'config_description3', 'post' );


		$return['config']['description4'] = $nv_Request->get_title( 'config_description4', 'post' );


		$return['config']['description5'] = $nv_Request->get_title( 'config_description5', 'post' );
		return $return;
	}

	function nv_menu_theme_content_left( $block_config )
	{
		global $global_config, $site_mods, $lang_global;

		if( file_exists( NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.content_left.tpl' ) )
		{
			$block_theme = $global_config['module_theme'];
		}
		elseif( file_exists( NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.content_left.tpl' ) )
		{
			$block_theme = $global_config['site_theme'];
		}
		else
		{
			$block_theme = 'default';
		}

		$xtpl = new XTemplate( 'global.content_left.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks' );
		$xtpl->assign( 'NV_BASE_SITEURL', NV_BASE_SITEURL );
        $xtpl->assign( 'LANG', $lang_global );
		$xtpl->assign( 'BLOCK_THEME', $block_theme );
		$xtpl->assign( 'DATA', $block_config );

		if( ! empty( $block_config['title1'] ) )
		{
			$xtpl->parse( 'main.title1' );
		}
		if( ! empty( $block_config['description1'] ) )
		{
			$xtpl->parse( 'main.description1' );
		}
		if( ! empty( $block_config['description2'] ) )
		{
			$xtpl->parse( 'main.description2' );
		}
		if( ! empty( $block_config['description3'] ) )
		{
			$xtpl->parse( 'main.description3' );
		}
		if( ! empty( $block_config['description4'] ) )
		{
			$xtpl->parse( 'main.description4' );
		}
		if( ! empty( $block_config['description5'] ) )
		{
			$xtpl->parse( 'main.description5' );
		}

		$xtpl->parse( 'main' );
		return $xtpl->text( 'main' );
	}
}

if( defined( 'NV_SYSTEM' ) )
{
	$content = nv_menu_theme_content_left( $block_config );
}