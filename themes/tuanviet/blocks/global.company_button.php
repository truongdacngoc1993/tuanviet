<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2014 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Sun, 04 May 2014 12:41:32 GMT
 */

if( ! defined( 'NV_MAINFILE' ) ) die( 'Stop!!!' );

if( ! nv_function_exists( 'nv_menu_theme_company_button' ) )
{
	function nv_menu_theme_company_button_config( $module, $data_block, $lang_block )
	{
		$html = '<tr>';
		$html .= '<td>' . $lang_block['tel'] . '</td>';
		$html .= '	<td><input type="text" name="config_tel" class="form-control" value="' . $data_block['tel'] . '"/></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['service'] . '</td>';
		$html .= '	<td><input type="text" name="config_service" class="form-control" value="' . $data_block['service'] . '"/></td>';
		$html .= '</tr>';
		$html .= '	<td>' . $lang_block['link_service'] . '</td>';
		$html .= '	<td><input type="text" name="config_link_service" class="form-control" value="' . $data_block['link_service'] . '"/></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['contact'] . '</td>';
		$html .= '	<td><input type="text" name="config_contact" class="form-control" value="' . $data_block['contact'] . '"/></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['link_contact'] . '</td>';
		$html .= '	<td><input type="text" name="config_link_contact" class="form-control" value="' . $data_block['link_contact'] . '"/></td>';
		$html .= '</tr>';
		return $html;
	}

	function nv_menu_theme_company_button_submit( $module, $lang_block )
	{
		global $nv_Request;
		$return = array();
		$return['error'] = array();
		$return['config']['tel'] = $nv_Request->get_title( 'config_tel', 'post' );
		$return['config']['service'] = $nv_Request->get_title( 'config_service', 'post' );
		$return['config']['link_service'] = $nv_Request->get_title( 'config_link_service', 'post' );
		$return['config']['contact'] = $nv_Request->get_title( 'config_contact', 'post' );
		$return['config']['link_contact'] = $nv_Request->get_title( 'config_link_contact', 'post' );
		return $return;
	}

	function nv_menu_theme_company_button( $block_config )
	{
		global $global_config, $site_mods, $lang_global;

		if( file_exists( NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.company_button.tpl' ) )
		{
			$block_theme = $global_config['module_theme'];
		}
		elseif( file_exists( NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.company_button.tpl' ) )
		{
			$block_theme = $global_config['site_theme'];
		}
		else
		{
			$block_theme = 'default';
		}

		$xtpl = new XTemplate( 'global.company_button.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks' );
		$xtpl->assign( 'NV_BASE_SITEURL', NV_BASE_SITEURL );
        $xtpl->assign( 'LANG', $lang_global );
		$xtpl->assign( 'BLOCK_THEME', $block_theme );
		$xtpl->assign( 'DATA', $block_config );

		if( ! empty( $block_config['tel'] ) )
		{
			$xtpl->parse( 'main.tel' );
		}
		if( ! empty( $block_config['service'] ) )
		{
			$xtpl->parse( 'main.service' );
		}
		if( ! empty( $block_config['contact'] ) )
		{
			$xtpl->parse( 'main.contact' );
		}
		if( ! empty( $block_config['link_service'] ) )
		{
			$xtpl->parse( 'main.link_service' );
		}
		if( ! empty( $block_config['link_contact'] ) )
		{
			$xtpl->parse( 'main.link_contact' );
		}

		$xtpl->parse( 'main' );
		return $xtpl->text( 'main' );
	}
}

if( defined( 'NV_SYSTEM' ) )
{
	$content = nv_menu_theme_company_button( $block_config );
}