<!-- BEGIN: main -->
<div class="button-support">
	<!-- BEGIN: tel -->
	<a href="tel:{DATA.tel}" class="btn btn-primary call"><i class="fa fa-phone"></i> {DATA.tel} </a>
	<!-- END: tel -->
	<a href="{DATA.link_service}" class="btn btn-primary call"> <!-- BEGIN: service --> <i class="fa fa-th"></i> {DATA.service} <!-- END: service --> </a>
	<a href="{DATA.link_contact}" class="btn btn-primary call"> <!-- BEGIN: contact --> <i class="fa fa-pencil"></i> {DATA.contact} <!-- END: contact --> </a>
</div>
<!-- END: main -->
