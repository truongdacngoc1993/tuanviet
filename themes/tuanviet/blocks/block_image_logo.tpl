<!-- BEGIN: main -->
<link rel="stylesheet" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/flexslider.css" type="text/css" media="screen" />
<script type="text/javascript" src="{NV_BASE_SITEURL}themes/{TEMPLATE}/js/jquery.flexisel.js"></script>
<ul id="flexiselDemo3">
	<!-- BEGIN: img_logo -->
	<li>
		<a href="{IMAGE.imglink}"><img  src="{NV_BASE_SITEURL}{IMAGE.imgpath}" height="100%" alt="{IMAGE.imgtitle}" title="{IMAGE.imgtitle}"/></a>
	</li>
	<!-- END: img_logo -->
</ul>
<script type="text/javascript">
	$(window).load(function() {
		$("#flexiselDemo3").flexisel({
			visibleItems : 5,
			animationSpeed : 1000,
			autoPlay : true,
			autoPlaySpeed : 1000,
			pauseOnHover : true,
			enableResponsiveBreakpoints : true,
			responsiveBreakpoints : {
				portrait : {
					changePoint : 480,
					visibleItems : 1
				},
				landscape : {
					changePoint : 640,
					visibleItems : 2
				},
				tablet : {
					changePoint : 768,
					visibleItems : 3
				}
			}
		});
	});
</script>
<!-- END: main -->