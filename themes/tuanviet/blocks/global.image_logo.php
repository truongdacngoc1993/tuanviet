<?php

/**
 * @Project NUKEVIET 3.0
 * @Author VINADES., JSC (contact@vinades.vn)
 * @Copyright (C) 2011 VINADES ., JSC. All rights reserved
 * @Createdate Jan 10, 2011  6:04:30 PM
 */

if ( ! defined( 'NV_MAINFILE' ) ) die( 'Stop!!!' );

if ( ! nv_function_exists( 'nv_block_global_image_logo' ) )
{
    function nv_block_data_config_image_logo( $module, $data_block, $lang_block )
    {
        global $lang_module;


        $imagecontent = $data_block['imagecontent'];

        $html = "";
        if ( ! empty( $imagecontent ) )
        {
            $imagecontent = explode( "-||-", $imagecontent );
        }
        else
        {
            $imagecontent = array();
        }
        $imagecontent = array_pad( $imagecontent, 10, '|||' );
        foreach ( $imagecontent as $key => $_imagecontent )
        {
            $_imagecontent = explode( "|", $_imagecontent );
            if ( ! empty( $_imagecontent[0] ) ) $_imagecontent[0] = NV_BASE_SITEURL . $_imagecontent[0];
            ++$key;
            $html .= '<tr><td>Image ' . $key . ' Path: </td><td><input style="width:380px" type="text" name="imagePath[]" id="imagePath' . $key . '" value="' . $_imagecontent[0] . '"/><input type="button" value="Browse server" name="selectimg"/></td></tr>';
            $html .= '<tr><td>Image ' . $key . ' Link: </td><td><input style="width:380px" type="text" name="imageLink[]" value="' . $_imagecontent[3] . '"/></td></tr>';
        }



        $html .= '<script type="text/javascript">
			//<![CDATA[
				$("input[name=selectimg]").click(function(){
					var area = $(this).prev().attr("id");
					var path= "' . NV_BASE_SITEURL . '";
					var type= "image";
					nv_open_browse(script_name + "?" + nv_name_variable + "=upload&popup=1&area=" + area+"&path="+path+"&type="+type, "NVImg", "850", "420","resizable=no,scrollbars=no,toolbar=no,location=no,status=no");
					return false;
				});
			//]]>
			</script>';

        return $html;
    }

    function nv_block_data_config_image_logo_submit( $module, $lang_block )
    {
        global $nv_Request;

        $imagePath = $nv_Request->get_array( 'imagePath', 'post', '' );
        $imageLink = $nv_Request->get_array( 'imageLink', 'post', '' );

        $images = array();
        for ( $i = 0; $i < 10; ++$i )
        {

                $link = nv_is_url( $imageLink[$i] ) ? $imageLink[$i] : "";

                $img = substr( $imagePath[$i], strlen( NV_BASE_SITEURL ) );
                $images[] = $img . "|" . $imageTitle[$i] . "|" . $imageDescription[$i] . "|" . $link;


        }

        $images = ! empty( $images ) ? implode( "-||-", $images ) : "";

        $return = array();
        $return['error'] = array();
        $return['config'] = array();
        $return['config']['imagecontent'] = $images;
        return $return;
    }

    function nv_block_global_image_logo( $block_config, $block_theme )
    {
    	 global $global_config;
        $imagecontent = $block_config['imagecontent'];

        if ( empty( $imagecontent ) ) return "";

        $imagecontent = explode( "-||-", $imagecontent );
        $images = array();

        foreach ( $imagecontent as $img )
        {
            $images[] = array_combine( array( 'imgpath', 'imgtitle', 'imgdescription', 'imglink' ), explode( "|", $img ) );
		}
        $imgcount = sizeof( $images );
        if ( empty( $imgcount ) ) return "";

        $xtpl = new XTemplate( "block_image_logo.tpl", NV_ROOTDIR . "/themes/" . $global_config['site_theme'] . "/blocks" );
        $xtpl->assign( 'NV_BASE_SITEURL', NV_BASE_SITEURL );
		$xtpl->assign( 'TEMPLATE', $block_theme );

        for( $i = 0; $i < $imgcount; $i++ )
		{

			if( !empty( $images[$i]['imgpath'] ) )
			{

				$xtpl->assign( 'IMAGE', $images[$i] );

                $xtpl->parse( 'main.img_logo' );;
            }
        }
        $xtpl->parse( 'main' );
        return $xtpl->text( 'main' );
    }
}

$content = "";

if ( defined( 'NV_SYSTEM' ) )
{
    global $site_mods, $module_name, $module_info, $my_head, $client_info,$global_config;

    $block_theme = "";

	if( file_exists( NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/block_image_logo.tpl' ) )
		{
			$block_theme = $global_config['module_theme'];
		}
		else
		{
			$block_theme = 'default';
	}

    if ( ! empty( $block_theme ) )
    {

        $content = nv_block_global_image_logo( $block_config, $block_theme );
    }
    else
    {
        $content = $module_info['template'] . "Khong tim thay file giao dien";
    }
}

?>