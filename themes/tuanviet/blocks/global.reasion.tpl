<!-- BEGIN: main -->
<ul >
	<!-- BEGIN: img -->

		<!-- BEGIN: style_1 -->
		<li >
			<img u="image" src="{IMAGE.imgpath}" />
			<!-- BEGIN: title -->
			<a>{IMAGE.imgtitle}</a>
			<!-- END: title -->
			<!-- BEGIN: imgdescription -->
			<p>{IMAGE.imgdescription}</p>
			<!-- END: imgdescription -->
		</li>
		<!-- END: style_1 -->

		<!-- BEGIN: style_2 -->
		<li >
			<img u="image" src="{IMAGE.imgpath}" />
			<!-- BEGIN: title -->
			<a>{IMAGE.imgtitle}</a>
			<!-- END: title -->
			<!-- BEGIN: imgdescription -->
			<p>{IMAGE.imgdescription}</p>
			<!-- END: imgdescription -->
		</li>
		<!-- END: style_2 -->


		<!-- BEGIN: style_3 -->
		<li >
			<img u="image" src="{IMAGE.imgpath}" />
			<!-- BEGIN: title -->
			<a>{IMAGE.imgtitle}</a>
			<!-- END: title -->
			<!-- BEGIN: imgdescription -->
			<p>{IMAGE.imgdescription}</p>
			<!-- END: imgdescription -->
		</li>
		<!-- END: style_3 -->

		<!-- BEGIN: style_4 -->
		<li >
			<img u="image" src="{IMAGE.imgpath}" />
			<!-- BEGIN: title -->
			<a>{IMAGE.imgtitle}</a>
			<!-- END: title -->
			<!-- BEGIN: imgdescription -->
			<p>{IMAGE.imgdescription}</p>
			<!-- END: imgdescription -->
		</li>
		<!-- END: style_4 -->
		<!-- END: img -->
</ul>
<!-- END: main -->