<!-- BEGIN: main -->
<ul>
	<!-- BEGIN: newloop -->
	<li class="clearfix col-md-8 col-sm-8 col-xs-8">
		<!-- BEGIN: imgblock -->
		<a title="{blocknews.title}"><img src="{blocknews.imgurl}" alt="{blocknews.title}" width="{blocknews.width}" class="img-thumbnail col-md-24"/></a>
		<!-- END: imgblock -->
		<h3 class="entry-title"><a data-content="{blocknews.hometext}" data-img="{blocknews.imgurl}" >{blocknews.title}</a></h3>
	</li>
	<!-- END: newloop -->
</ul>
<!-- END: main -->
