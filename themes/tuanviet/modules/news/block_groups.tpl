<!-- BEGIN: main -->
<ul class="last-post">
	<!-- BEGIN: loop -->
	<li class="clearfix col-md-8 col-sm-8 col-xs-8">
		<h3 class="entry-title"><a href="{ROW.link}" data-content="{ROW.hometext}" data-img="{ROW.thumb}">{ROW.title}</a></h3>
		<!-- BEGIN: img -->
		<a href="{ROW.link}" title="{ROW.title}"><img src="{ROW.thumb}" alt="{ROW.title}" width="{ROW.blockwidth}" class="img-thumbnail pull-left"/></a>
		<!-- END: img -->
	</li>
	<!-- END: loop -->
</ul>
<!-- END: main -->