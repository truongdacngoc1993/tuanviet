<!-- BEGIN: main -->
<ul class="list-unstyled">
	<!-- BEGIN: loop -->
	<li class="clearfix thumbnail1">
		<!-- BEGIN: img -->
		<a href="{ROW.link}" title="{ROW.title}"><img src="{ROW.thumb}" alt="{ROW.title}" width="{ROW.blockwidth}" class="img-thumbnail pull-left"/></a>
		<!-- END: img -->
		<h3 class="entry-title"><a href="{ROW.link}" title="{ROW.title}">{ROW.title}</a> </h3>
	</li>
	<!-- END: loop -->
</ul>
<!-- END: main -->